/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticket.domain;

/**
 *
 * @author User
 */
public class FlightForTrip {
    
    private Flight inBoundFlight;
    private Flight outBoundFlight;

    public FlightForTrip(Flight inboundFlight, Flight outboundFlight) 
    {
         
         inBoundFlight = inboundFlight;
         outBoundFlight = outboundFlight;
    }

    public Flight getInBoundFlight() {
        return inBoundFlight;
    }

    public Flight getOutBoundFlight() {
        return outBoundFlight;
    }
    
}
