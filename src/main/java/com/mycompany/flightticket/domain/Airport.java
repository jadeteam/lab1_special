/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticket.domain;

/**
 *
 * @author User
 */
public enum Airport {
    
    BKK ("Suvarnabhumi Airport","Bangkok(Bang Phli, Samut Prakan)","BKK","TH"),
    DMK("Don Mueang International Airport","Bangkok","DMK","TH"),
    HKT("Phuket International Airport","Phuket","HKT","TH"),
    CNX("Chiang Mai International Airport","Chiang Mai","CNX","TH"),
    HDY("Hat Yai International Airport","Songkhla","HDY","TH"),
    CEI("Mae Fah Luang Chiang Rai International Airport","Chiang","HKT","TH"),
    LHR("London Heathrow Airport","London","LHR","ENG");
    
    private final String airportName;
    private final String cityName;
    private final String cityInitial;
    private final String countryInitial;
    
    Airport(String airportName, String cityName, String cityInitial, String countryInitial)
    {
        this.airportName = airportName;
        this.cityName = cityName;
        this.cityInitial = cityInitial;
        this.countryInitial = countryInitial;
        
    }
    
    public String getAirportName()
    {
        return airportName;
    }
    public String getCityName()
    {
        return cityName;
    }
    public String getCityInitial()
    {
        return cityInitial;
    }
    public String getCountryInitial()
    {
        return countryInitial;
    }
    
    
    
}
