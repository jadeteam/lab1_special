/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticket.domain;


import java.util.Calendar;

/**
 *
 * @author User
 */
public class Flight {
   
         private String flightNumber;
         private AirCraftType aircraftType;
         private AirLine operator;
         private Calendar departureTime;
         private Calendar arrivalTime;
         private Airport fromAirport;
         private Airport toAirport;
         private boolean inBound;
         private int remainingSeats;
         private String departureTerminal;
         private String arrivalTerminal;
         private int numberOfTerminal;
         private FareRate fare;

    public String getFlightNumber() {
        return flightNumber = "TG910";
    }

    public AirCraftType getAircraftType() {
        return aircraftType = AirCraftType.A380;
    }

    public AirLine getOperator() {
        return operator = AirLine.THAI_AIRWAY;
    }

    public Calendar getDepartureTime() {
        return departureTime;
    }

    public Calendar getArrivalTime() {
        return arrivalTime;
    }

    public Airport getFromAirport() {
        return fromAirport = Airport.BKK;
    }

    public Airport getToAirport() {
        return toAirport = Airport.LHR;
    }

    public boolean isInBound() {
        return inBound;
    }

    public int getRemainingSeats() {
        return remainingSeats = 9;
    }

    public String getDepartureTerminal() {
        return departureTerminal = "Terminal";
    }

    public String getArrivalTerminal() {
        return arrivalTerminal = "Terminal 1";
    }

    public int getNumberOfTerminal() {
        return numberOfTerminal;
    }

    public FareRate getFare() {
        return fare = FareRate.H;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setAircraftType(AirCraftType aircraftType) {
        this.aircraftType = aircraftType;
    }

    public void setOperator(AirLine operator) {
        this.operator = operator;
    }

    public void setDepartureTime(Calendar departureTime) {
        this.departureTime = departureTime;
    }

    public void setArrivalTime(Calendar arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setFromAirport(Airport fromAirport) {
        this.fromAirport = fromAirport;
    }

    public void setToAirport(Airport toAirport) {
        this.toAirport = toAirport;
    }

    public void setInBound(boolean inBound) {
        this.inBound = inBound;
    }

    public void setRemainingSeats(int remainingSeats) {
        this.remainingSeats = remainingSeats;
    }

    public void setDepartureTerminal(String departureTerminal) {
        this.departureTerminal = departureTerminal;
    }

    public void setArrivalTerminal(String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
    }

    public void setNumberOfTerminal(int numberOfTerminal) {
        this.numberOfTerminal = numberOfTerminal;
    }

    public void setFare(FareRate fare) {
        this.fare = fare;
    }
         
    
         
}
