/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticket.controller;

import com.mycompany.flightticket.domain.Flight;
import com.mycompany.flightticket.domain.FlightForTrip;
import com.mycompany.flightticket.domain.Trip;
import java.util.List;

/**
 *
 * @author User
 */
public interface IFlightManagement {
    
    List<FlightForTrip> searchFlight(Trip trip) throws MissingRequiredTripInfoException;
    
    
}
